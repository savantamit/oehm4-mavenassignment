package com.oehm4.mavenassignment;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Association {
	public void saveAppsDetails(Apps apps) {
		Configuration configuration = new Configuration();
		configuration.configure("config.xml");
		configuration.addAnnotatedClass(Apps.class);
	 	SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(apps);
		transaction.commit();	
	}
	public Apps getAppDetailsById(Long id) {
		Configuration configuration = new Configuration();
		configuration.configure("config.xml");
		configuration.addAnnotatedClass(Apps.class);
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		return session.get(Apps.class, id);
	}
	public void updateVersionById(Long id,String version) {
		Apps app = getAppDetailsById(id);
		if(app != null) {
			Configuration configuration = new Configuration();
			configuration.configure("config.xml");
			configuration.addAnnotatedClass(Apps.class);
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			app.setVersion(version);
			session.update(app);
			transaction.commit();
			System.out.println("version updated sucessfully ");
		}else {
			System.out.println("version Update failed");
		}
	}
}
